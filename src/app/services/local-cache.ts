import { Injectable } from '@angular/core';
import { Constants } from './constants';

@Injectable()

export class LocalCacheService {
    private setItem(key, value) {
        window.localStorage.setItem(key, value);
    }

    public getItem(key) {
        return window.localStorage.getItem(key);
    }

    private clearItem(key) {
        window.localStorage.removeItem(key);
    }
    public setLoginData(data) {
        this.setItem(Constants.STORAGE_ITEM.TOKEN, data.token);
        this.setItem(Constants.STORAGE_ITEM.LOGIN_DATA, JSON.stringify(data));
        // this.setItem(Constants.STORAGE_ITEM.GMT, JSON.stringify(data.timezone));
    }

    setGmtTimeZone(timeZone) {
        this.setItem(Constants.STORAGE_ITEM.GMT, timeZone);
    }
    public getGmtTimeZone() {
        return this.getItem(Constants.STORAGE_ITEM.GMT);
    }


    public setEditId(key,value) {
        this.setItem(key, value);
    }


    public getLoginData() {
        return JSON.parse(this.getItem(Constants.STORAGE_ITEM.LOGIN_DATA));
    }


   
    public getToken() {
        return this.getItem(Constants.STORAGE_ITEM.TOKEN);
    }

    public getCurrentUser() {
        return this.getLoginData();
    }

    public isUserLoggedIn() {
        return this.getToken() != null;
    }
    public logoutUser() {
        this.clearItem(Constants.STORAGE_ITEM.LOGIN_DATA);
        this.clearItem(Constants.STORAGE_ITEM.TOKEN);
        this.clearItem(Constants.STORAGE_ITEM.GMT);
        this.setItem('isExpire', 0);
    }
    public logoutSessionExpireUser() {
        this.clearItem(Constants.STORAGE_ITEM.LOGIN_DATA);
        this.clearItem(Constants.STORAGE_ITEM.TOKEN);
        this.clearItem(Constants.STORAGE_ITEM.GMT);
        this.setItem('isExpire', 1);
    }
}