import { Injectable } from '@angular/core';
import { Constants } from './constants';
import { Http, Headers } from '@angular/http';
import { LocalCacheService } from './local-cache';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class ApiService {

    constructor(private http: Http, private localCache: LocalCacheService) { }

    getFormattedUrl(route) {
        let url = Constants.BASE_URL + route;
        return url;
    }

    private get(request) {
        return this.http.get(this.getFormattedUrl(request.route), { headers: request.headers });
    }

    private post(request) {
        return this.http.post(this.getFormattedUrl(request.route), request.body, { headers: request.headers });
    }

    private put(request) {
        return this.http.put(this.getFormattedUrl(request.route), request.body, { headers: request.headers });
    }

    private delete(request) {
        return this.http.delete(this.getFormattedUrl(request.route), { headers: request.headers });
    }

    generateApiRequest(request, isTokened = false) {
        let response;
        if (!request.headers) {
            request.headers = new Headers();
        }
        if (isTokened) {
            request.headers.append('Token', this.localCache.getToken());
        }
        switch (request.type) {
            case Constants.REQUEST_TYPE.GET:
                response = this.get(request);
                break;
            case Constants.REQUEST_TYPE.POST:
                response = this.post(request);
                break;
            case Constants.REQUEST_TYPE.PUT:
                response = this.put(request);
                break;
            case Constants.REQUEST_TYPE.DELETE:
                response = this.delete(request);
                break;
        }
        return response;
    }

}
