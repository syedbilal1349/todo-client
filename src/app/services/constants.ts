export class Constants {
    public static BASE_URL = 'http://localhost:6002/api/';;
    public static SERVER_URL = 'http://localhost:6002/';
    public static CHAT_SERVER_URL = 'http://localhost:3000/';

    public static REQUEST_TYPE = {
        GET: 'GET',
        POST: 'POST',
        PUT: 'PUT',
        DELETE: 'DELETE',
    };

    public static API_ROUTES = {
        LOGIN: { route: 'auth/login', type: Constants.REQUEST_TYPE.PUT },
        ADD_USER: { route: 'user', type: Constants.REQUEST_TYPE.POST },
        ADD_TODO: { route: 'todo', type: Constants.REQUEST_TYPE.POST },
        TODO_DETAILS: { route: 'todo/', type: Constants.REQUEST_TYPE.GET },
        GET_ALL_TODO: { route: 'todo/user/', type: Constants.REQUEST_TYPE.GET },
        UPDATE_TODO: { route: 'todo', type: Constants.REQUEST_TYPE.PUT },
        DELETE_TODO: { route: 'todo/', type: Constants.REQUEST_TYPE.DELETE },
    };

    public static VIEW_ROUTES = {
        DASHBOARD: '/dashboard',
        LOGIN: '/auth/login',
        UPDATE_TODO: '/todo/edit',
        LIST_TODO: "/todo"
    };

    public static STORAGE_ITEM = {
        LOGIN_DATA: '_LUD',
        TOKEN: '_UT',
        GMT: 'GMT_TIMEZONE'
    };

}