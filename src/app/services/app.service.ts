import { Injectable } from '@angular/core';
import { Constants } from './constants';
import { ApiService } from './api.services';
import { LocalCacheService } from "./local-cache";
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import swal from 'sweetalert2';

@Injectable()
export class AppService {

    items: any;
    constructor(private apiService: ApiService, private router: Router, private localCache: LocalCacheService) { }

    public navigateToView(path, id = null) {
        if (id != null) {
            this.router.navigate([path, id]);
        } else {
            this.router.navigate([path]);
        }
    }

    public showSimpleAlert(title, text, type) {
        swal({
            title,
            text,
            type,
        }).catch(swal.noop);
    }


    public login(user) {
        return new Promise((resolve, reject) => {
            this.apiService.generateApiRequest({ route: Constants.API_ROUTES.LOGIN.route, type: Constants.API_ROUTES.LOGIN.type, body: user }, true).map(res => res.json()).subscribe(response => {
                resolve(response);
            });
        });
    }

    public addUser(user) {
        return new Promise((resolve, reject) => {
            this.apiService.generateApiRequest({ route: Constants.API_ROUTES.ADD_USER.route, type: Constants.API_ROUTES.ADD_USER.type, body: user }, true).map(res => res.json()).subscribe(response => {
                resolve(response);
            });
        });
    }



    public addTodo(todo) {
        return new Promise((resolve, reject) => {
            this.apiService.generateApiRequest({ route: Constants.API_ROUTES.ADD_TODO.route, type: Constants.API_ROUTES.ADD_TODO.type, body: todo }, true).map(res => res.json()).subscribe(response => {
                resolve(response);
            });
        });
    }

    public fetchAllTodos(id) {
        return new Promise((resolve, reject) => {
            this.apiService.generateApiRequest({ route: Constants.API_ROUTES.GET_ALL_TODO.route + id, type: Constants.API_ROUTES.GET_ALL_TODO.type }, true).map(res => res.json()).subscribe(response => {
                resolve(response);
            });
        });
    }

    public fetchTodoDetails(id) {
        return new Promise((resolve, reject) => {
            this.apiService.generateApiRequest({ route: Constants.API_ROUTES.TODO_DETAILS.route + id, type: Constants.API_ROUTES.TODO_DETAILS.type }, true).map(res => res.json()).subscribe(response => {
                resolve(response);
            });
        });
    }

    public updateTodo(todo) {
        return new Promise((resolve, reject) => {
            this.apiService.generateApiRequest({ route: Constants.API_ROUTES.UPDATE_TODO.route, type: Constants.API_ROUTES.UPDATE_TODO.type, body: todo }, true).map(res => res.json()).subscribe(response => {
                resolve(response);
            });
        });
    }

    public deleteTodo(id) {
        return new Promise((resolve, reject) => {
            this.apiService.generateApiRequest({ route: Constants.API_ROUTES.DELETE_TODO.route + id, type: Constants.API_ROUTES.DELETE_TODO.type }, true).map(res => res.json()).subscribe(response => {
                resolve(response);
            });
        });
    }

    public groupBy(data, key) {
        const result = data.reduce(function (r, a) {
            r[a[key]] = r[a[key]] || [];
            r[a[key]].push(a);
            return r;
        }, Object.create(null));
        return result;
    }

    public relativeTimeDifferenceFromNow(current, previous) {
        const msPerMinute = 60 * 1000;
        const msPerHour = msPerMinute * 60;
        const msPerDay = msPerHour * 24;
        const msPerMonth = msPerDay * 30;
        const msPerYear = msPerDay * 365;

        const elapsed = current - previous;

        if (elapsed < msPerMinute) {
            return Math.round(elapsed / 1000) + ' seconds ago';
        } else if (elapsed < msPerHour) {
            return Math.round(elapsed / msPerMinute) + ' minutes ago';
        } else if (elapsed < msPerDay) {
            return Math.round(elapsed / msPerHour) + ' hours ago';
        } else if (elapsed < msPerMonth) {
            return Math.round(elapsed / msPerDay) + ' days ago';
        } else if (elapsed < msPerYear) {
            return Math.round(elapsed / msPerMonth) + ' months ago';
        } else {
            return Math.round(elapsed / msPerYear) + ' years ago';
        }
    }

    public getDayEndTime(timestamp) {
        const date = new Date(timestamp);
        date.setHours(23, 59, 59, 59);
        return date;
    }

    public getDayStartTime(timestamp) {
        const date = new Date(timestamp);
        date.setHours(0, 0, 0, 0);
        return date;
    }

    public getFormattedDate(timestamp) {
        const date = new Date(timestamp);
        const dateString = (date.getMonth() + 1) + '/' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
        return dateString;
    }

    public getTimeForGMT(gmt) {
        const d = new Date();
        const utc = d.getTime() + (d.getTimezoneOffset() * 60000);
        let nd = new Date(utc + (3600000 * gmt)).toString();
        nd = nd.split('G')[0];
        return nd;
    }

    public ChangeTimeForGMT(gmt, givenDate) {
        const d = new Date(givenDate);
        const utc = d.getTime() + (d.getTimezoneOffset() * 60000);
        let nd = new Date(utc + (3600000 * gmt)).toString();
        nd = nd.split('G')[0];
        return nd;
    }

    public getRandomColor() {
        const letters = '0123456789ABCDEF';
        let color = '#';
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

}
