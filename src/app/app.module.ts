import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { Ng2OdometerModule } from 'ng2-odometer';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { AppComponent } from './app.component';
import { AdminComponent } from './layout/admin/admin.component';
import { AuthComponent } from './layout/auth/auth.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { MenuItems } from './shared/menu-items/menu-items';
import { BreadcrumbsComponent } from './layout/admin/breadcrumbs/breadcrumbs.component';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CanActivateAuthLayoutGuard } from './services/guards/can-activate-auth-layout.gaurd';
import { CanActivateHomeLayoutGuard } from './services/guards/can-activate-home-layout.guard';

import { LocalCacheService } from './services/local-cache';
import { ApiService } from './services/api.services';
import { AppService } from './services/app.service';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    AuthComponent,
    BreadcrumbsComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2OdometerModule.forRoot(),
    Ng4LoadingSpinnerModule.forRoot(),
    NgxDatatableModule
  ],
  providers: [MenuItems, CanActivateAuthLayoutGuard, CanActivateHomeLayoutGuard, LocalCacheService, AppService, ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
