import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultComponent } from './default.component';
import { DefaultRoutingModule } from './default-routing.module';
import { SharedModule } from '../../../shared/shared.module';
import { Ng2OdometerModule } from 'ng2-odometer';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';





@NgModule({
  imports: [
    CommonModule,
    DefaultRoutingModule,
    SharedModule,
    // SimpleNotificationsModule.forRoot(),
    Ng2OdometerModule,
    Ng4LoadingSpinnerModule.forRoot(),
  ],
  // providers: [
  //   {
  //     provide: HighchartsStatic,
  //     useFactory: highchartsFactory
  //   }
  // ],
  declarations: [DefaultComponent],
  bootstrap: [DefaultComponent]
})
export class DefaultModule { }
