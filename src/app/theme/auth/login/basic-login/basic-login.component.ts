import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../services/app.service';
import { LocalCacheService } from '../../../../services/local-cache';
import { Constants } from '../../../../services/constants';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';


@Component({
  selector: 'app-basic-login',
  templateUrl: './basic-login.component.html',
  styleUrls: ['./basic-login.component.scss']
})
export class BasicLoginComponent implements OnInit {

  user: any = {
    email: '',
    password: '',
  };

  remember : boolean = false
  
  constructor(private appService: AppService, private localCache: LocalCacheService, private spinnerService: Ng4LoadingSpinnerService) { }

  ngOnInit() {
  }

  loginUser() {
    this.user.rememberme = this.remember
    this.spinnerService.show();
    this.appService.login(this.user).then((resp: any) => {
      if (resp.success) {
        this.spinnerService.hide();
        this.localCache.setLoginData(resp.data);
        this.localCache.setGmtTimeZone(resp.data.timezone);
        this.spinnerService.hide();
        this.appService.navigateToView(Constants.VIEW_ROUTES.LIST_TODO);
      } else {
        this.spinnerService.hide();
        this.appService.showSimpleAlert('Authentication', resp.message, 'error');
      }
    });
  }
}
