import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../services/app.service';
import { LocalCacheService } from '../../../services/local-cache';
import { Constants } from '../../../services/constants';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  user: any = {
    name: '',
    email: '',
    password: '',
    confirmPassword: '',
  };

  isValidate: boolean = true

  constructor(private appService: AppService, private localCache: LocalCacheService, private spinnerService: Ng4LoadingSpinnerService) { }


  ngOnInit() {
  }

  addUser() {
    this.appService.addUser(this.user).then((resp: any) => {
      this.appService.showSimpleAlert("User", resp.message, resp.success ? 'success' : 'error');
      if (resp.success) {
        this.appService.navigateToView(Constants.VIEW_ROUTES.LOGIN)
      }
    })
  }

} 
