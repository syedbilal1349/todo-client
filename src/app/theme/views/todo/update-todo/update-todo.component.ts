import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AppService } from '../../../../services/app.service';
import { LocalCacheService } from '../../../../services/local-cache';
import { Constants } from '../../../../services/constants';

@Component({
  selector: 'app-update-todo',
  templateUrl: './update-todo.component.html',
  styleUrls: ['./update-todo.component.scss']
})
export class UpdateTodoComponent implements OnInit {

  updateTodoForm: FormGroup
  todoId: any
  todoDetails: any = {}

  constructor(private appService: AppService, private LocalCache: LocalCacheService, private spinnerService: Ng4LoadingSpinnerService) {
    this.create_form()
  }


  ngOnInit() {
    this.todoId = this.LocalCache.getItem('todoId')
    this.fetchTodoDetails()
  }

  create_form() {
    this.updateTodoForm = new FormGroup({
      title: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
    })
  }

  fetchTodoDetails() {
    this.spinnerService.show()
    this.appService.fetchTodoDetails(this.todoId).then((resp: any) => {
      if (resp.success) {
        this.todoDetails = resp.data
        this.setTodoDetails()
        this.spinnerService.hide()
      }
    })
  }

  setTodoDetails() {
    this.updateTodoForm.get('title').setValue(this.todoDetails.title)
    this.updateTodoForm.get('description').setValue(this.todoDetails.description)
  }

  updateTodo() {
    this.spinnerService.show()
    this.updateTodoForm.value.id = this.todoId;
    if(this.updateTodoForm.value.title == this.todoDetails.title){
      delete this.updateTodoForm.value.title
    }
    this.appService.updateTodo(this.updateTodoForm.value).then((resp: any) => {
      this.appService.showSimpleAlert("Todo", resp.message, resp.success ? 'success' : 'error');
      if (resp.success) {
        this.spinnerService.hide()
        this.appService.navigateToView(Constants.VIEW_ROUTES.LIST_TODO)
      }
    })
  }
}
