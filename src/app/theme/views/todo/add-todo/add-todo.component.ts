import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AppService } from '../../../../services/app.service';
import { LocalCacheService } from '../../../../services/local-cache';
import { Constants } from '../../../../services/constants';


@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.scss']
})
export class AddTodoComponent implements OnInit {
  addTodoForm: FormGroup
  todos: any = []
  p: number = 1
  userId: any
  constructor(private appService: AppService, private LocalCache: LocalCacheService, private spinnerService: Ng4LoadingSpinnerService) {
    this.create_form()
  }

  ngOnInit() {
    this.userId = this.LocalCache.getLoginData()._id
    this.fetchAllTodos()
  }

  create_form() {
    this.addTodoForm = new FormGroup({
      title: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
    })
  }

  addTodo() {
    this.appService.addTodo(this.addTodoForm.value).then((resp: any) => {
      this.appService.showSimpleAlert("Todo", resp.message, resp.success ? 'success' : 'error');
      if (resp.success) {
        this.addTodoForm.reset()
        this.fetchAllTodos()
      }
    })
  }

  fetchAllTodos() {
    this.appService.fetchAllTodos(this.userId).then((resp: any) => {
      if (resp) {
        this.todos = resp.data
      }
    })
  }

  updateTodo(id) {
    this.LocalCache.setEditId('todoId', id)
    this.appService.navigateToView(Constants.VIEW_ROUTES.UPDATE_TODO)
  }

  markCompleted(id){
    this.appService.updateTodo({id : id , isCompleted : true}).then((resp:any)=>{
      this.appService.showSimpleAlert("Todo", "ToDo marked as completed!", resp.success ? 'success' : 'error');
      if(resp.success){
        this.fetchAllTodos()
      }
    })
  }

  deleteTodo(id) {
    swal({
      title: 'Are you sure?',
      text: 'You wont be able to revert',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.appService.deleteTodo(id).then((resp: any) => {
          if (resp) {
            swal(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            );
            this.fetchAllTodos()
          }
        })
      }
    });
  }
}

