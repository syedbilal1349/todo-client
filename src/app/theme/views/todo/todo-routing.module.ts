import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddTodoComponent } from './add-todo/add-todo.component';
import { UpdateTodoComponent } from './update-todo/update-todo.component';

const routes: Routes = [
  {
      path: '',
      data: {
          title: 'Todo',
          status: false
      },
      children: [
          {
              path: '',
              component: AddTodoComponent,
          },
          {
            path: 'edit',
            component: UpdateTodoComponent,
        },
      ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TodoRoutingModule { }
