import {Directive, HostListener} from '@angular/core';

import * as screenfull from "screenfull";
import {Screenfull} from "screenfull";

@Directive({
  selector: '[appToggleFullScreen]'
})
export class ToggleFullScreenDirective {

  @HostListener('click')
  sf = <Screenfull>screenfull;
  onClick() {
    if (this.sf.enabled) {
      this.sf.toggle();
    }
  }
}
