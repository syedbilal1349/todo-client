import { Injectable } from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  short_label?: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMS = [
  {
    label: 'ToDos',
    main: [
      {
        state: 'todo',
        short_label: 'A',
        name: 'Todo',
        type: 'link',
        icon: 'ti-bell',
      },
    ],
  }
];



@Injectable()
export class MenuItems {
  getAdminMenu(): Menu[] {
    return MENUITEMS;
  }
}
