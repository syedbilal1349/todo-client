import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './layout/admin/admin.component';
import { AuthComponent } from './layout/auth/auth.component';
import { CanActivateHomeLayoutGuard } from './services/guards/can-activate-home-layout.guard';
import { CanActivateAuthLayoutGuard } from './services/guards/can-activate-auth-layout.gaurd';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      },
      {
        path: 'todo',
        loadChildren: './theme/views/todo/todo.module#TodoModule'
      },
    ],
    canActivate: [CanActivateHomeLayoutGuard]
  },
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: 'auth',
        loadChildren: './theme/auth/auth.module#AuthModule'
      }
    ],
    canActivate: [CanActivateAuthLayoutGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
