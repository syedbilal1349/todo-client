# TODO APPLICATION

## Prerequisites

To run the application on your local server, you need to go in the directory of the project then open the terminal in the respective folder then enter command "npm install" and hit enter. This will install all the dependencies of the project that you need to run it.

## Configuration

To change the configuration opne the constants.ts file in the project directory. From here you can find the IP of your Base Server and all the API Endpoints.

## Run

Open terminal in your project directory and enter "npm start". This command will start your development server on "localhost:4200".
ENJOY! 







